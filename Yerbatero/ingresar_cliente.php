<?php
$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "yerba";

// Crear conexión
$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Variables para manejar las acciones
$accion = isset($_POST['accion']) ? $_POST['accion'] : '';

// Agregar cliente
if ($accion == 'crear' && $_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST['nombre'];
    $telefono = $_POST['telefono'];
    $correo = $_POST['correo'];
    $localidad = $_POST['localidad'];

    $sql = "INSERT INTO clientes (nombre, telefono, correo, localidad) VALUES (?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssss", $nombre, $telefono, $correo, $localidad);

    if ($stmt->execute()) {
        echo "<script>alert('Cliente agregado exitosamente');</script>";
    } else {
        echo "<script>alert('Error al agregar cliente');</script>";
    }
}

$conn->close();
?>

<h2>Formulario de Agregar Cliente</h2>
<form method="POST" action="">
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre" required><br>

    <label for="telefono">Teléfono:</label>
    <input type="text" id="telefono" name="telefono" required><br>

    <label for="correo">Correo:</label>
    <input type="email" id="correo" name="correo" required><br>

    <label for="localidad">Localidad:</label>
    <input type="text" id="localidad" name="localidad" required><br>

    <!-- Acción de Crear -->
    <input type="hidden" name="accion" value="crear">
    <button type="submit">Agregar Cliente</button>
</form>

<hr>

<!-- Botón para volver a la lista de clientes -->
<button class="button" onclick="window.location.href='clientes.php'">Volver</button>
