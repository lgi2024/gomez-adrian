<?php
$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "yerba";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

$tipo = $_POST['tipo'];
$cantidad = $_POST['cantidad'];
$accion = $_POST['accion'];

if ($accion == "ingresar") {
    $checkSql = "SELECT * FROM materia_prima WHERE tipo = '$tipo'";
    $checkResult = $conn->query($checkSql);

    if ($checkResult->num_rows > 0) {
        $sql = "UPDATE materia_prima SET ingreso = ingreso + $cantidad, total = total + $cantidad WHERE tipo = '$tipo'";
    } else {
        $sql = "INSERT INTO materia_prima (tipo, ingreso, salido, total) VALUES ('$tipo', $cantidad, 0, $cantidad)";
    }
} elseif ($accion == "sacar") {
    $checkSql = "SELECT * FROM materia_prima WHERE tipo = '$tipo'";
    $checkResult = $conn->query($checkSql);

    if ($checkResult->num_rows > 0) {
        $row = $checkResult->fetch_assoc();
        // Verificar que hay suficiente cantidad en total para restar
        if ($row['total'] >= $cantidad) {
            $sql = "UPDATE materia_prima SET salido = salido + $cantidad, total = total - $cantidad WHERE tipo = '$tipo'";
        } else {
            echo "<p style='color: red;'>Error: No hay suficiente cantidad para sacar.</p>";
            exit();
        }
    } else {
        echo "<p style='color: red;'>Error: El tipo de materia prima no existe.</p>";
        exit();
    }
}

if ($conn->query($sql) === TRUE) {
    header("Location: materia_prima.php");
    exit();
} else {
    echo "<p style='color: red;'>Error: " . $conn->error . "</p>";
}

$conn->close();
?>
