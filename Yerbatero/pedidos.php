<?php
$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "yerba";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['accion']) && $_POST['accion'] == 'actualizar_estado') {
    $id_producto = $_POST['Id_producto'];
    $nuevo_estado = $_POST['estado'];

    $sql = "UPDATE pedidos SET estado = ? WHERE Id_producto = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("si", $nuevo_estado, $id_producto);

    if ($stmt->execute()) {
        echo "<script>alert('Estado actualizado exitosamente'); window.location.href='pedidos.php';</script>";
    } else {
        echo "<script>alert('Error al actualizar el estado');</script>";
    }
    $stmt->close();
}

$sql = "SELECT p.Id_producto, p.producto, p.cantidad, c.nombre AS cliente, p.fecha_entrega, p.estado 
        FROM pedidos p 
        JOIN clientes c ON p.Id_cliente = c.Id_cliente";
$result = $conn->query($sql);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Pedidos</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f8f9fa;
            color: #343a40;
        }
        .container {
            width: 80%;
            margin: auto;
            text-align: center;
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }
        h2 {
            color: #007bff;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        table, th, td {
            border: 1px solid #dee2e6;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        th {
            background-color: #007bff;
            color: #ffffff;
        }
        .button {
            margin: 10px;
            padding: 10px 20px;
            cursor: pointer;
            border: none;
            border-radius: 5px;
            color: #ffffff;
            background-color: #007bff;
            transition: background-color 0.3s;
        }
        .button:hover {
            background-color: #0056b3;
        }
        .form-section {
            margin-top: 30px;
        }
        input[type="text"], input[type="number"], input[type="date"] {
            width: 80%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #dee2e6;
            border-radius: 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <h2>Lista de Pedidos</h2>
    <table>
        <tr>
            <th>Id Producto</th>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Cliente</th>
            <th>Fecha de Entrega</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
        <?php
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>
                        <td>{$row['Id_producto']}</td>
                        <td>{$row['producto']}</td>
                        <td>{$row['cantidad']}</td>
                        <td>{$row['cliente']}</td>
                        <td>{$row['fecha_entrega']}</td>
                        <td>{$row['estado']}</td>
                        <td>
                            <form action='pedidos.php' method='post' style='display:inline;'>
                                <input type='hidden' name='accion' value='actualizar_estado'>
                                <input type='hidden' name='Id_producto' value='{$row['Id_producto']}'>
                                <select name='estado'>
                                    <option value='Pendiente' " . ($row['estado'] == 'Pendiente' ? 'selected' : '') . ">Pendiente</option>
                                    <option value='En Proceso' " . ($row['estado'] == 'En Proceso' ? 'selected' : '') . ">En Proceso</option>
                                    <option value='Completado' " . ($row['estado'] == 'Completado' ? 'selected' : '') . ">Completado</option>
                                </select>
                                <button type='submit' class='button'>Actualizar Estado</button>
                            </form>
                        </td>
                      </tr>";
            }
        } else {
            echo "<tr><td colspan='7'>No hay pedidos disponibles</td></tr>";
        }
        ?>
    </table>
    <button class="button" onclick="window.location.href='ingresar_pedido.php'">Agregar Pedido</button>
    <button class="button" onclick="window.location.href='opciones2.php'">Volver</button>

</div>

<?php $conn->close(); ?>
</body>
</html>
