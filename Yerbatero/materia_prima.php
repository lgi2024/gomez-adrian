<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Materia Prima</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f8f9fa;
            color: #343a40;
        }
        .container {
            width: 60%;
            margin: auto;
            text-align: center;
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }
        h2 {
            color: #007bff;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        table, th, td {
            border: 1px solid #dee2e6;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        th {
            background-color: #007bff;
            color: #ffffff;
        }
        .button {
            margin: 10px;
            padding: 10px 20px;
            cursor: pointer;
            border: none;
            border-radius: 5px;
            color: #ffffff;
            background-color: #007bff;
            transition: background-color 0.3s;
        }
        .button:hover {
            background-color: #0056b3;
        }
        .modal {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            align-items: center;
            justify-content: center;
        }
        .modal-content {
            background-color: #fff;
            padding: 20px;
            width: 300px;
            text-align: left;
            border-radius: 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <h2>Inventario de Materia Prima</h2>

    <table>
        <tr>
            <th>Id</th>
            <th>Tipo</th>
            <th>Ingreso</th>
            <th>Salida</th>
            <th>Total</th>
        </tr>
        <?php
        $conn = new mysqli("localhost", "alumno", "", "yerba");

        if ($conn->connect_error) {
            die("Conexión fallida: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM materia_prima";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>
                        <td>{$row['Id']}</td>
                        <td>{$row['tipo']}</td>
                        <td>{$row['ingreso']}</td>
                        <td>{$row['salido']}</td>
                        <td>{$row['total']}</td>
                      </tr>";
            }
        } else {
            echo "<tr><td colspan='5'>No hay datos disponibles</td></tr>";
        }

        $conn->close();
        ?>
    </table>

    <button class="button" onclick="openModal('ingreso')">Ingresar Materia Prima</button>
    <button class="button" onclick="openModal('salida')">Sacar Materia Prima</button>
    <br>
    <button class="button" onclick="window.location.href='opciones.php'">Volver</button>
</div>

<div id="modalIngreso" class="modal">
    <div class="modal-content">
        <h3>Ingresar Materia Prima</h3>
        <form action="procesar_materia_prima.php" method="post">
            <input type="hidden" name="accion" value="ingresar">
            <label for="tipo">Tipo:</label>
            <input type="text" id="tipo" name="tipo" required><br><br>
            <label for="cantidad">Cantidad:</label>
            <input type="number" id="cantidad" name="cantidad" required><br><br>
            <button type="submit" class="button">Guardar</button>
            <button type="button" class="button" onclick="closeModal('ingreso')">Cancelar</button>
        </form>
    </div>
</div>

<div id="modalSalida" class="modal">
    <div class="modal-content">
        <h3>Sacar Materia Prima</h3>
        <form action="procesar_materia_prima.php" method="post">
            <input type="hidden" name="accion" value="sacar">
            <label for="tipo">Tipo:</label>
            <input type="text" id="tipo" name="tipo" required><br><br>
            <label for="cantidad">Cantidad:</label>
            <input type="number" id="cantidad" name="cantidad" required><br><br>
            <button type="submit" class="button">Guardar</button>
            <button type="button" class="button" onclick="closeModal('salida')">Cancelar</button>
        </form>
    </div>
</div>

<script>
    function openModal(modalId) {
        document.getElementById('modal' + capitalize(modalId)).style.display = 'flex';
    }
    function closeModal(modalId) {
        document.getElementById('modal' + capitalize(modalId)).style.display = 'none';
    }
    function capitalize(word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }
</script>

</body>
</html>
