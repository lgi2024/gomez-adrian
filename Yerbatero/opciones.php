<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Opciones</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f2f2f2;
            font-family: Arial, sans-serif;
            position: relative;
        }
        .option-container {
            text-align: center;
        }
        .option-boxes {
            display: flex;
            gap: 20px;
            justify-content: center;
            margin-top: 20px;
        }
        .option-box {
            padding: 20px;
            color: white;
            border-radius: 10px;
            text-align: center;
            width: 150px;
            cursor: pointer;
        }
        .materia-prima {
            background-color: green;
        }
        .comercializacion {
            background-color: #FFA500;
        }
        .option-box:hover {
            opacity: 0.8;
        }
        .image-container img {
            width: 500px;
            height: auto;
            margin-bottom: 20px;
            margin-top: -100px;
        }
        .logout-button {
            position: absolute;
            top: 20px;
            right: 20px;
            background-color: #d9534f;
            color: white;
            padding: 10px 15px;
            border-radius: 5px;
            text-decoration: none;
            font-weight: bold;
        }
        .logout-button:hover {
            background-color: #c9302c;
        }
    </style>
</head>
<body>
<a href="logout.php" class="logout-button">Cerrar Sesión</a>
    <div class="option-container">
        <div class="image-container">
            <img src="yerba.jpg" alt="Imagen de Yerba">
        </div>
        <div class="option-boxes">
            <div class="option-box materia-prima" onclick="window.location.href='materia_prima.php'">Materia Prima</div>
            <div class="option-box comercializacion" onclick="window.location.href='opciones2.php'">Comercialización</div>
        </div>
    </div>
</body>
</html>
