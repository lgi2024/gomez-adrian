<?php
$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "yerba";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['accion']) && $_POST['accion'] == 'actualizar_estado') {
    $Id_estado = $_POST['Id_estado'];
    $nuevo_estado = $_POST['estado'];

    $sql = "UPDATE estado_cliente SET estado = ? WHERE Id_estado = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("si", $nuevo_estado, $Id_estado);

    if ($stmt->execute()) {
        echo "<script>alert('Estado actualizado exitosamente'); window.location.href='estado_cliente.php';</script>";
    } else {
        echo "<script>alert('Error al actualizar el estado');</script>";
    }
    $stmt->close();
}


$sql = "SELECT ec.Id_estado, ec.estado, c.nombre AS cliente 
        FROM estado_cliente ec 
        JOIN clientes c ON ec.Id_cliente = c.Id_cliente";
$result = $conn->query($sql);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Deudas</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f8f9fa;
            color: #343a40;
        }
        .container {
            width: 80%;
            margin: auto;
            text-align: center;
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }
        h2 {
            color: #007bff;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        table, th, td {
            border: 1px solid #dee2e6;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        th {
            background-color: #007bff;
            color: #ffffff;
        }
        .button {
            margin: 30px;
            padding: 10px 20px;
            cursor: pointer;
            border: none;
            border-radius: 10px;
            color: #ffffff;
            background-color: #007bff;
            transition: background-color 0.3s;
        }
        .button:hover {
            background-color: #0056b3;
        }
        .form-section {
            margin-top: 30px;
        }
        input[type="text"], input[type="number"], input[type="date"] {
            width: 80%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #dee2e6;
            border-radius: 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <h2>Estado de Clientes</h2>
    <table>
        <tr>
            <th>ID Estado</th>
            <th>Cliente</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
        <?php if ($result->num_rows > 0): ?>
            <?php while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td><?= $row['Id_estado'] ?></td>
                    <td><?= $row['cliente'] ?></td>
                    <td><?= $row['estado'] ?></td>
                    <td>
                        <form method="post">
                            <input type="hidden" name="Id_estado" value="<?= $row['Id_estado'] ?>">
                            <input type="hidden" name="accion" value="actualizar_estado">
                            <select name="estado" required>
                                <option value="Al Día" <?= $row['estado'] === 'Al Día' ? 'selected' : '' ?>>Al Día</option>
                                <option value="Debe" <?= $row['estado'] === 'Debe' ? 'selected' : '' ?>>Debe</option>
                            </select>
                            <button type="submit">Actualizar</button>
                        </form>
                    </td>
                </tr>
            <?php endwhile; ?>
        <?php else: ?>
            <tr><td colspan="4">No hay datos disponibles</td></tr>
        <?php endif; ?>
    </table>
</div>

    <button class="button" onclick="window.location.href='actualizar_estado.php'">Agregar Cliente</button>
    <button class="button" onclick="window.location.href='opciones2.php'">Volver</button>

<?php $conn->close(); ?>
</body>
</html>
