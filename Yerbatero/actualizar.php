<?php
$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "yerba";

// Crear conexión
$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar conexión
if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

$id_cliente = isset($_GET['Id_cliente']) ? $_GET['Id_cliente'] : 0;
$cliente = null;

// Obtener datos del cliente
if ($id_cliente > 0) {
    $sql = "SELECT * FROM clientes WHERE Id_cliente = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id_cliente);
    $stmt->execute();
    $result = $stmt->get_result();
    
    if ($result->num_rows > 0) {
        $cliente = $result->fetch_assoc();
    } else {
        echo "<p>Cliente no encontrado.</p>";
    }
    $stmt->close();
} else {
    echo "<p>ID de cliente no válido.</p>";
}

$conn->close();
?>

<h2>Formulario de Actualización de Cliente</h2>
<?php if ($cliente): ?>
    <form method="POST" action="clientes.php">
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" value="<?= htmlspecialchars($cliente['nombre']) ?>" required><br>

        <label for="telefono">Teléfono:</label>
        <input type="text" id="telefono" name="telefono" value="<?= htmlspecialchars($cliente['telefono']) ?>" required><br>

        <label for="correo">Correo:</label>
        <input type="email" id="correo" name="correo" value="<?= htmlspecialchars($cliente['correo']) ?>" required><br>

        <label for="localidad">Localidad:</label>
        <input type="text" id="localidad" name="localidad" value="<?= htmlspecialchars($cliente['localidad']) ?>" required><br>

        <input type="hidden" name="accion" value="actualizar">
        <input type="hidden" name="Id_cliente" value="<?= $cliente['Id_cliente'] ?>">

        <button type="submit">Actualizar Cliente</button>
    </form>
<?php endif; ?>

<hr>

<button class="button" onclick="window.location.href='clientes.php'">Volver</button>
