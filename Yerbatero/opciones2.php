<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Opciones</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f2f2f2;
            font-family: Arial, sans-serif;
            position: relative;
        }
        .option-container {
            text-align: center;
        }
        .option-boxes {
            display: flex;
            gap: 20px;
            justify-content: center;
            margin-top: 20px;
        }
        .option-box {
            padding: 20px;
            color: white;
            border-radius: 10px;
            text-align: center;
            width: 150px;
            cursor: pointer;
        }
        .clientes {
            background-color: green;
        }
        .pedidos {
            background-color: #FFA500;
        }
        .estado-clientes {
            background-color: #007bff;
        }
        .option-box:hover {
            opacity: 0.8;
        }
        .image-container img {
            width: 500px;
            height: auto;
            margin-bottom: 20px;
            margin-top: -100px;
        }
        .logout-button {
            position: absolute;
            top: 20px;
            right: 20px;
            background-color: #d9534f;
            color: white;
            padding: 10px 15px;
            border-radius: 5px;
            text-decoration: none;
            font-weight: bold;
        }
        .logout-button:hover {
            background-color: #c9302c;
        }
    </style>
</head>
<body>
<a href="logout.php" class="logout-button">Cerrar Sesión</a>
    <div class="option-container">
        <div class="image-container">
            <img src="yerba.jpg" alt="Imagen de Yerba">
        </div>
        <div class="option-boxes">
            <div class="option-box clientes" onclick="window.location.href='clientes.php'">Clientes</div>
            <div class="option-box pedidos" onclick="window.location.href='pedidos.php'">Pedidos</div>
            <div class="option-box estado-clientes" onclick="window.location.href='estado_cliente.php'">Estado Clientes</div>
            <button class="button" onclick="window.location.href='opciones.php'">Volver</button>
        </div>
    </div>
</body>
</html>
