<?php
$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "yerba";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $accion = $_POST['accion'] ?? '';

    if ($accion === 'eliminar') {
        $id_cliente = $_POST['Id_cliente'];
        $sql = "DELETE FROM clientes WHERE Id_cliente = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $id_cliente);
        
        if ($stmt->execute()) {
            echo "<script>alert('Cliente eliminado exitosamente'); window.location.href='clientes.php';</script>";
        } else {
            echo "<script>alert('Error al eliminar cliente');</script>";
        }
        $stmt->close();
    } elseif ($accion === 'actualizar') {
        $id_cliente = $_POST['Id_cliente'];
        $nombre = $_POST['nombre'];
        $telefono = $_POST['telefono'];
        $correo = $_POST['correo'];
        $localidad = $_POST['localidad'];

        $sql = "UPDATE clientes SET nombre = ?, telefono = ?, correo = ?, localidad = ? WHERE Id_cliente = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ssssi", $nombre, $telefono, $correo, $localidad, $id_cliente);

        if ($stmt->execute()) {
            echo "<script>alert('Cliente actualizado exitosamente'); window.location.href='clientes.php';</script>";
        } else {
            echo "<script>alert('Error al actualizar cliente');</script>";
        }
        $stmt->close();
    }
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Clientes</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f8f9fa;
            color: #343a40;
        }
        .container {
            width: 60%;
            margin: auto;
            text-align: center;
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }
        h2 {
            color: #007bff;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        table, th, td {
            border: 1px solid #dee2e6;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        th {
            background-color: #007bff;
            color: #ffffff;
        }
        .button {
            margin: 10px;
            padding: 10px 20px;
            cursor: pointer;
            border: none;
            border-radius: 5px;
            color: #ffffff;
            background-color: #007bff;
            transition: background-color 0.3s;
        }
        .button:hover {
            background-color: #0056b3;
        }
        .form-section {
            margin-top: 30px;
        }
        input[type="text"], input[type="email"] {
            width: 80%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #dee2e6;
            border-radius: 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <h2>Lista de Clientes</h2>
    <table>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Teléfono</th>
            <th>Correo</th>
            <th>Localidad</th>
            <th>Acciones</th>
        </tr>
        <?php
        $sql = "SELECT * FROM clientes";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>
                        <td>{$row['Id_cliente']}</td>
                        <td>{$row['nombre']}</td>
                        <td>{$row['telefono']}</td>
                        <td>{$row['correo']}</td>
                        <td>{$row['localidad']}</td>
                        <td>
                            <form action='clientes.php' method='post' style='display:inline;'>
                                <input type='hidden' name='accion' value='eliminar'>
                                <input type='hidden' name='Id_cliente' value='{$row['Id_cliente']}'>
                                <button type='submit' class='button' onclick=\"return confirm('¿Estás seguro de que deseas eliminar este cliente?')\">Eliminar</button>
                            </form>
                            <form action='actualizar.php' method='get' style='display:inline;'>
                                <input type='hidden' name='Id_cliente' value='{$row['Id_cliente']}'>
                                <button type='submit' class='button'>Actualizar</button>
                            </form>
                        </td>
                      </tr>";
            }
        } else {
            echo "<tr><td colspan='6'>No hay datos disponibles</td></tr>";
        }
        ?>
    </table>

    <button class="button" onclick="window.location.href='ingresar_cliente.php'">Agregar Cliente</button>
    <button class="button" onclick="window.location.href='opciones2.php'">Volver</button>
</div>

<?php $conn->close(); ?>
</body>
</html>
