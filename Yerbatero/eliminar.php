<?php
// Conexión a la base de datos
include('conexion.php');

// Lógica para eliminar cliente
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['accion']) && $_POST['accion'] == 'eliminar') {
    $id_cliente = $_POST['Id_cliente'];

    $sql = "DELETE FROM clientes WHERE Id_cliente = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id_cliente);

    if ($stmt->execute()) {
        echo "<script>alert('Cliente eliminado exitosamente'); window.location.href='clientes.php';</script>";
    } else {
        echo "<script>alert('Error al eliminar cliente');</script>";
    }
}
?>

<!-- Formulario para eliminar cliente -->
<h3>Eliminar Cliente</h3>
<form method="POST" action="">
    <label for="Id_cliente">ID Cliente a Eliminar:</label>
    <input type="number" id="Id_cliente" name="Id_cliente" required><br>

    <!-- Acción de eliminar -->
    <input type="hidden" name="accion" value="eliminar">
    <button type="submit" onclick="return confirm('¿Estás seguro de que deseas eliminar este cliente?');">Eliminar Cliente</button>
</form>
