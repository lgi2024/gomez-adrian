<?php
$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "yerba";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id_estado = $_POST['Id_estado'];
    $id_cliente = $_POST['Id_cliente'];
    $nombre = $_POST['nombre'];
    $estado = $_POST['estado'];

    $estado_cliente_query = $conn->query("SELECT nombre FROM estado_cliente WHERE Id_estado = $id_estado");
    $estado_cliente_row = $estado_cliente_query->fetch_assoc();
    $estado_cliente = $estado_cliente_row['nombre'];

    $cliente_query = $conn->query("SELECT nombre FROM clientes WHERE Id_cliente = $id_cliente");
    $cliente_row = $cliente_query->fetch_assoc();
    $nombre_cliente = $cliente_row['nombre'];

    $sql = "INSERT INTO estado_cliente (Id_estado, nombre, estado) VALUES (?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("isissss", $id_estado, $nombre, $estado);

    if ($stmt->execute()) {
        echo "<script>alert('Cliente agregado exitosamente'); window.location.href='estado_clientes.php';</script>";
    } else {
        echo "<script>alert('Error al agregar el cliente');</script>";
    }
    $stmt->close();
}

$clientes = $conn->query("SELECT Id_cliente, nombre FROM clientes");

$conn->close();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ingresar Pedido</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f8f9fa;
            color: #343a40;
        }
        .container {
            width: 50%;
            margin: auto;
            text-align: center;
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }
        h2 {
            color: #007bff;
        }
        label, select, input {
            display: block;
            width: 80%;
            margin: 10px auto;
            padding: 8px;
        }
        .button {
            margin: 10px;
            padding: 10px 20px;
            cursor: pointer;
            border: none;
            border-radius: 5px;
            color: #ffffff;
            background-color: #007bff;
            transition: background-color 0.3s;
        }
        .button:hover {
            background-color: #0056b3;
        }
        .suggestions {
        background-color: #fff;
        border: 1px solid #ddd;
        max-height: 150px;
        overflow-y: auto;
        width: 80%;
        margin: auto;
        position: absolute;
        z-index: 1000;
    }
    .suggestion-item {
        padding: 10px;
        cursor: pointer;
    }
    .suggestion-item:hover {
        background-color: #f1f1f1;
    }
    </style>

<script>
        function filtrarProductos() {
            let input = document.getElementById("busquedaProducto").value.toLowerCase();
            let opciones = document.getElementById("Id_producto").options;

            for (let i = 0; i < opciones.length; i++) {
                let texto = opciones[i].text.toLowerCase();
                if (texto.includes(input)) {
                    opciones[i].style.display = "";
                } else {
                    opciones[i].style.display = "none";
                }
            }
        }

        function filtrarClientes() {
            let input = document.getElementById("busquedaCliente").value.toLowerCase();
            let opciones = document.getElementById("Id_cliente").options;

            for (let i = 0; i < opciones.length; i++) {
                let texto = opciones[i].text.toLowerCase();
                if (texto.includes(input)) {
                    opciones[i].style.display = "";
                } else {
                    opciones[i].style.display = "none";
                }
            }
        }

    </script>

</head>
<body>

<div class="container">
    <h2>Agregar Estado de Cliente</h2>
    <form method="POST" action="">

        <label for="busquedaClientes">Buscar Cliente:</label>
        <input type="text" id="busquedaClientes" onkeyup="filtrarClientes()" placeholder="Buscar cliente..." autocomplete="off">
        <div id="sugerenciasClientes" class="suggestions"></div>
        <input type="hidden" id="Id_cliente" name="Id_cliente" required>

        <label for="estado">Estado:</label>
        <select id="estado" name="estado" required>
            <option value="Pendiente">Debe</option>
            <option value="En Proceso">Al Dia</option>
        </select>

        <button type="submit" class="button">Agregar Cliente</button>
    </form>

    <button class="button" onclick="window.location.href='estado_cliente.php'">Volver</button>
</div>

<script>
    const clientes = <?= json_encode($clientes->fetch_all(MYSQLI_ASSOC)) ?>;
    const inputBusquedaCliente = document.getElementById("busquedaClientes");
    const sugerenciasDivCliente = document.getElementById("sugerenciasClientes");
    const idClienteInput = document.getElementById("Id_cliente");

    function filtrarClientes() {
        const termino = inputBusquedaCliente.value.toLowerCase();
        sugerenciasDivCliente.innerHTML = "";

        if (termino) {
            const clientesFiltrados = clientes.filter(cliente => 
                cliente.nombre.toLowerCase().includes(termino)
            );

            clientesFiltrados.forEach(cliente => {
                const item = document.createElement("div");
                item.classList.add("suggestion-item");
                item.textContent = cliente.nombre;
                item.onclick = () => seleccionarCliente(cliente);
                sugerenciasDivCliente.appendChild(item);
            });
        }
    }
    function seleccionarCliente(cliente) {
        inputBusquedaCliente.value = cliente.nombre;
        idClienteInput.value = cliente.Id_cliente;
        sugerenciasDivCliente.innerHTML = "";
    }
</script>

</body>
</html>
