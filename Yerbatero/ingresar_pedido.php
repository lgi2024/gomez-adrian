<?php
$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "yerba";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id_producto = $_POST['Id_producto'];
    $cantidad = $_POST['cantidad'];
    $id_cliente = $_POST['Id_cliente'];
    $fecha_entrega = $_POST['fecha_entrega'];
    $estado = $_POST['estado'];

    $producto_query = $conn->query("SELECT producto FROM productos WHERE Id_producto = $id_producto");
    $producto_row = $producto_query->fetch_assoc();
    $producto = $producto_row['producto'];

    $cliente_query = $conn->query("SELECT nombre FROM clientes WHERE Id_cliente = $id_cliente");
    $cliente_row = $cliente_query->fetch_assoc();
    $nombre_cliente = $cliente_row['nombre'];

    $sql = "INSERT INTO pedidos (Id_producto, producto, cantidad, Id_cliente, nombre, fecha_entrega, estado) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("isissss", $id_producto, $producto, $cantidad, $id_cliente, $nombre_cliente, $fecha_entrega, $estado);

    if ($stmt->execute()) {
        echo "<script>alert('Pedido agregado exitosamente'); window.location.href='pedidos.php';</script>";
    } else {
        echo "<script>alert('Error al agregar pedido');</script>";
    }
    $stmt->close();
}


$productos = $conn->query("SELECT Id_producto, producto FROM productos");
$clientes = $conn->query("SELECT Id_cliente, nombre FROM clientes");

$conn->close();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ingresar Pedido</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f8f9fa;
            color: #343a40;
        }
        .container {
            width: 50%;
            margin: auto;
            text-align: center;
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }
        h2 {
            color: #007bff;
        }
        label, select, input {
            display: block;
            width: 80%;
            margin: 10px auto;
            padding: 8px;
        }
        .button {
            margin: 10px;
            padding: 10px 20px;
            cursor: pointer;
            border: none;
            border-radius: 5px;
            color: #ffffff;
            background-color: #007bff;
            transition: background-color 0.3s;
        }
        .button:hover {
            background-color: #0056b3;
        }
        .suggestions {
        background-color: #fff;
        border: 1px solid #ddd;
        max-height: 150px;
        overflow-y: auto;
        width: 80%;
        margin: auto;
        position: absolute;
        z-index: 1000;
    }
    .suggestion-item {
        padding: 10px;
        cursor: pointer;
    }
    .suggestion-item:hover {
        background-color: #f1f1f1;
    }
    </style>

<script>
        function filtrarProductos() {
            let input = document.getElementById("busquedaProducto").value.toLowerCase();
            let opciones = document.getElementById("Id_producto").options;

            for (let i = 0; i < opciones.length; i++) {
                let texto = opciones[i].text.toLowerCase();
                if (texto.includes(input)) {
                    opciones[i].style.display = "";
                } else {
                    opciones[i].style.display = "none";
                }
            }
        }

        function filtrarClientes() {
            let input = document.getElementById("busquedaCliente").value.toLowerCase();
            let opciones = document.getElementById("Id_cliente").options;

            for (let i = 0; i < opciones.length; i++) {
                let texto = opciones[i].text.toLowerCase();
                if (texto.includes(input)) {
                    opciones[i].style.display = "";
                } else {
                    opciones[i].style.display = "none";
                }
            }
        }

    </script>

</head>
<body>

<div class="container">
    <h2>Agregar Nuevo Pedido</h2>
    <form method="POST" action="">
        
        <label for="busquedaProducto">Buscar Producto:</label>
        <input type="text" id="busquedaProducto" onkeyup="filtrarProductos()" placeholder="Buscar producto..." autocomplete="off">
        <div id="sugerencias" class="suggestions"></div>
        <input type="hidden" id="Id_producto" name="Id_producto" required>

        <label for="cantidad">Cantidad:</label>
        <input type="number" id="cantidad" name="cantidad" min="1" required>

        <label for="busquedaClientes">Buscar Cliente:</label>
        <input type="text" id="busquedaClientes" onkeyup="filtrarClientes()" placeholder="Buscar cliente..." autocomplete="off">
        <div id="sugerenciasClientes" class="suggestions"></div>
        <input type="hidden" id="Id_cliente" name="Id_cliente" required>


        <label for="fecha_entrega">Fecha de Entrega:</label>
        <input type="date" id="fecha_entrega" name="fecha_entrega" required>

        <label for="estado">Estado:</label>
        <select id="estado" name="estado" required>
            <option value="Pendiente">Pendiente</option>
            <option value="En Proceso">En Proceso</option>
            <option value="Completado">Completado</option>
        </select>

        <button type="submit" class="button">Agregar Pedido</button>
    </form>

    <button class="button" onclick="window.location.href='pedidos.php'">Volver</button>
</div>

<script>
    const productos = <?= json_encode($productos->fetch_all(MYSQLI_ASSOC)) ?>;
    const inputBusqueda = document.getElementById("busquedaProducto");
    const sugerenciasDiv = document.getElementById("sugerencias");
    const idProductoInput = document.getElementById("Id_producto");

    function filtrarProductos() {
        const termino = inputBusqueda.value.toLowerCase();
        sugerenciasDiv.innerHTML = "";

        if (termino) {
            const productosFiltrados = productos.filter(producto => 
                producto.producto.toLowerCase().includes(termino)
            );

            productosFiltrados.forEach(producto => {
                const item = document.createElement("div");
                item.classList.add("suggestion-item");
                item.textContent = producto.producto;
                item.onclick = () => seleccionarProducto(producto);
                sugerenciasDiv.appendChild(item);
            });
        }
    }

    function seleccionarProducto(producto) {
        inputBusqueda.value = producto.producto;
        idProductoInput.value = producto.Id_producto;
        sugerenciasDiv.innerHTML = "";
    }

    const clientes = <?= json_encode($clientes->fetch_all(MYSQLI_ASSOC)) ?>;
    const inputBusquedaCliente = document.getElementById("busquedaClientes");
    const sugerenciasDivCliente = document.getElementById("sugerenciasClientes");
    const idClienteInput = document.getElementById("Id_cliente");

    function filtrarClientes() {
        const termino = inputBusquedaCliente.value.toLowerCase();
        sugerenciasDivCliente.innerHTML = "";

        if (termino) {
            const clientesFiltrados = clientes.filter(cliente => 
                cliente.nombre.toLowerCase().includes(termino)
            );

            clientesFiltrados.forEach(cliente => {
                const item = document.createElement("div");
                item.classList.add("suggestion-item");
                item.textContent = cliente.nombre;
                item.onclick = () => seleccionarCliente(cliente);
                sugerenciasDivCliente.appendChild(item);
            });
        }
    }

    // Función para seleccionar cliente
    function seleccionarCliente(cliente) {
        inputBusquedaCliente.value = cliente.nombre;
        idClienteInput.value = cliente.Id_cliente;
        sugerenciasDivCliente.innerHTML = "";
    }
</script>

</body>
</html>
