<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Agregar Usuario</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f2f2f2;
            font-family: Arial, sans-serif;
        }
        .login-container {
            text-align: center;
            background-color: white;
            padding: 30px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
            width: 300px;
        }
        h2 {
            margin-bottom: 20px;
            font-size: 20px;
        }
        label {
            font-size: 16px;
        }
        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-size: 14px;
        }
        input[type="submit"] {
            width: 100%;
            padding: 10px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 5px;
            font-size: 16px;
            cursor: pointer;
        }
        input[type="submit"]:hover {
            background-color: #45a049;
        }
        .button {
            margin: 10px;
            padding: 10px 20px;
            cursor: pointer;
            border: none;
            border-radius: 5px;
            color: #ffffff;
            background-color: #007bff;
            transition: background-color 0.3s;
        }
    </style>
</head>
<body>
    <div class="login-container">
        <h2>Agregar Nuevo Usuario</h2>
        <form action="guardar_usuario.php" method="post">
            <label for="NuevoUsuario">Usuario:</label>
            <input type="text" id="NuevoUsuario" name="usuario" required><br><br>
            
            <label for="NuevaContrasena">Contraseña:</label>
            <input type="password" id="NuevaContrasena" name="contrasena" required><br><br>
            
            <input type="submit" value="Guardar Usuario">
            <br><br>
    <button class="button" onclick="window.location.href='Inicio_de_Sesion.html'">Volver</button>
        </form>
    </div>
</body>
</html>
