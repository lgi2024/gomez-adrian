<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Guardar Usuario</title>
    <style>
        /* Estilos para el mensaje */
        .message-container {
            width: 300px;
            margin: 100px auto;
            padding: 20px;
            border-radius: 10px;
            text-align: center;
            font-family: Arial, sans-serif;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
        }
        .message-success {
            background-color: #dff0d8;
            color: #3c763d;
        }
        .message-error {
            background-color: #f2dede;
            color: #a94442;
        }
        a {
            display: block;
            margin-top: 10px;
            color: #007bff;
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <?php
    $servername = "localhost";
    $username = "alumno";
    $password = "";
    $dbname = "yerba";

    $conn = mysqli_connect($servername, $username, $password, $dbname);

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $usuario = $_POST['usuario'];
        $contrasena = $_POST['contrasena'];

        $sql = "INSERT INTO persona (usuario, contrasena) VALUES ('$usuario', '$contrasena')";

        if (mysqli_query($conn, $sql)) {
            echo '<div class="message-container message-success">';
            echo '<p>Usuario agregado exitosamente.</p>';
            echo '<a href="comercializacion.php">Ir a Comercialización</a>';
            echo '</div>';
        } else {
            echo '<div class="message-container message-error">';
            echo '<p>Error al agregar el usuario: ' . mysqli_error($conn) . '</p>';
            echo '<a href="AgregarUsuario.html">Regresar a Agregar Usuario</a>';
            echo '</div>';
        }
        
    }

    mysqli_close($conn);
    ?>
</body>
</html>
