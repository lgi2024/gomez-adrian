<?php

$servername = "localhost";
$username = "alumno";
$password = "";
$dbname = "Lenguaje";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$total = 0;

if (isset($_POST['productos'])) {
    $productos_seleccionados = $_POST['productos'];

    foreach ($productos_seleccionados as $id) {
        $sql = "SELECT Valores FROM Productos WHERE id = $id";
        $result = mysqli_query($conn, $sql);

        if ($result && mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);
            $total += $row["Valores"];
        }
    }
} else {
    die("No ha seleccionado ningún producto. <br><br><a href='http://localhost/gomez-adrian/Pedidos/Formulario_Productos.php'><button>Volver</button></a>");
}

if (isset($_POST['metodo'])) {
    $metodo = $_POST['metodo'];
    if ($metodo == "Delivery") {
        $total *= 1.10;
    }
} else {
    die("No ha seleccionado método de entrega. <br><br><a href='http://localhost/gomez-adrian/Pedidos/Formulario_Productos.php'><button>Volver</button></a>");
}

echo "<h2>Su pedido fue tomado con éxito</h2>";
echo "Total a pagar: $" . number_format($total, 2);

mysqli_close($conn);
?>
