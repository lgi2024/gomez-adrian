<?php
$conn = mysqli_connect('localhost', 'alumno', '', 'escuela');

if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}

// Crear
if (isset($_POST['crear'])) {
    $name = $_POST['nombre'];
    $age = $_POST['edad'];
    $email = $_POST['correo'];

    $sql = "INSERT INTO estudiantes (nombre, edad, correo) VALUES ('$name', $age, '$email')";
    if ($age<0){
        echo "Error: No puede ser negativa la edad.";
    }else{
        $sql = "INSERT INTO estudiantes (nombre, edad, correo) VALUES ('$name', $age, '$email')";

        if (mysqli_query($conn, $sql)) {
            echo "Nuevo estudiante creado con éxito";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
    }

}

if (isset($_GET['enviar'])){
    $busqueda = $_GET['busqueda'];

    $sql = "SELECT * FROM estudiantes WHERE nombre LIKE '%$busqueda%'";
    $resultado=mysqli_query($conn, $sql);

    if ($resultado) {
        echo "Resultado de la búsqueda.<br>";
        while ($fila = mysqli_fetch_assoc($resultado)) {
            echo "ID: " . $fila['id'] . "<br>";
            echo "Nombre: " . $fila['nombre'] . "<br>";
            echo "Edad: " . $fila['edad'] . "<br>";
            echo "Correo: " . $fila['correo'] . "<br><br>";
        }
    } else {
        echo "Error: " . mysqli_error($conn) . "<br>";
    }
}
    


// Actualizar
if (isset($_POST['actualizar'])) {
    $id = $_POST['id'];
    $name = $_POST['nombre'];
    $age = $_POST['edad'];
    $email = $_POST['correo'];

    $sql = "UPDATE estudiantes SET nombre='$name', edad=$age, correo='$email' WHERE id=$id";
    if ($age<0){
        echo "Error: No puede ser negativa la edad.";
    }else{
        $sql = "UPDATE estudiantes SET nombre='$name', edad=$age, correo='$email' WHERE id=$id";
        
    if (mysqli_query($conn, $sql)) {
        echo "Estudiantes actualizado con éxito";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
    }
}

// Eliminar
if (isset($_POST['eliminar'])) {
    $id = $_POST['id'];

    $sql = "DELETE FROM estudiantes WHERE id=$id";

    if (mysqli_query($conn, $sql)) {
        echo "Estudiante eliminado con éxito";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}


// Consultar por ID
$estudiante = null;
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $sql = "SELECT id, nombre, edad, correo FROM estudiantes WHERE id=$id";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $estudiante = mysqli_fetch_assoc($result);
    } else {
        echo "No se encontró el estudiante con ID: $id";
    }
}

// Consultar
$sql = "SELECT id, nombre, edad, correo FROM estudiantes";
$result = mysqli_query($conn, $sql);

mysqli_close($conn);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Gestión de Estudiantes</title>
</head>
<body>

<h1>Gestión de Estudiantes</h1>


<h2>Crear Estudiante</h2>
<form method="post" action="">
    Nombre: <input type="text" name="nombre" required><br>
    Edad: <input type="number" name="edad" required><br>
    Correo electrónico: <input type="email" name="correo" required><br>
    <input type="submit" name="crear" value="Crear">
</form>

<br>
<html>
<body>
<form action=""method="get">
    <input type="text" name="busqueda"><br>
    <input type="submit" name="enviar" value="Buscar">
</form>
<br><br><br>

<?php if ($estudiante): ?>
<h2>Actualizar Estudiante</h2>
<form method="post" action="">
    <input type="hidden" name="id" value="<?php echo $estudiante['id']; ?>">
    Nombre: <input type="text" name="nombre" value="<?php echo $estudiante['nombre']; ?>" required><br>
    Edad: <input type="number" name="edad" value="<?php echo $estudiante['edad']; ?>" required><br>
    Correo electrónico: <input type="email" name="correo" value="<?php echo $estudiante['correo']; ?>" required><br>
    <input type="submit" name="actualizar" value="Actualizar">
    <input type="submit" name="eliminar" value="Eliminar">
    <a href="http://localhost/gomez-adrian/Estudiantes/Estudiantes.php?id=2"><button type="button">Cancelar</button></a>
</form>
<?php endif; ?>


<h2>Lista de Estudiantes</h2>
<?php if (mysqli_num_rows($result) > 0): ?>
    <table border='1'>
        <tr><th>ID</th><th>Nombre</th><th>Edad</th><th>Correo Electrónico</th></tr>
        <?php while ($row = mysqli_fetch_assoc($result)): ?>
            <tr>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['nombre']; ?></td>
                <td><?php echo $row['edad']; ?></td>
                <td><?php echo $row['correo']; ?></td>
                <td><a href="?id=<?php echo $row['id']; ?>">Editar/Eliminar</a></td>
            </tr>
        <?php endwhile; ?>
    </table>
<?php else: ?>
    <p>No hay estudiantes registrados.</p>
<?php endif; ?>

</body>
</html>