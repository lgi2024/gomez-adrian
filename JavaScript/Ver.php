<?php
session_start();
require 'config.php';

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario_id'])) {
    header('Location: Inicio_de_sesion.php');
    exit;
}

$usuario_id = $_SESSION['usuario_id'];
$rol = $_SESSION['rol'];

// Orden
$search = isset($_GET['search']) ? $_GET['search'] : '';
$sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'nombre';
$order = isset($_GET['order']) ? $_GET['order'] : 'ASC';

// Consulta para obtener los estudiantes
$sql = "SELECT id, nombre, edad, correo, rol, foto FROM estudiantes WHERE nombre LIKE ? ORDER BY $sort_by $order";
$stmt = $conn->prepare($sql);
$search_param = "%" . $search . "%";
$stmt->bind_param('s', $search_param);
$stmt->execute();
$result = $stmt->get_result();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Lista de Estudiantes</title>
    <style>
        body {
            background-color: #f0f0f0;
            text-align: center;
            padding: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            padding: 10px;
            border: 1px solid #ddd;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        .button {
            background-color: #4CAF50;
            color: white;
            border: none;
            padding: 10px 20px;
            text-align: center;
            font-size: 16px;
            cursor: pointer;
            border-radius: 5px;
            margin: 10px;
        }
    </style>
    <script>
        // Función para filtrar
        function buscarEstudiantes() {
            let input = document.getElementById("busqueda").value.toLowerCase();
            let filas = document.getElementsByTagName("tr");

            for (let i = 1; i < filas.length; i++) {
                let nombre = filas[i].getElementsByTagName("td")[1].innerText.toLowerCase();
                let apellido = filas[i].getElementsByTagName("td")[2].innerText.toLowerCase();

                if (nombre.includes(input) || apellido.includes(input)) {
                    filas[i].style.display = "";  
                } else {
                    filas[i].style.display = "none";  
                }
            }
        }
    </script>
</head>
<body>

<h1>Lista de Estudiantes</h1>

<form method="GET" action="Ver.php">
    <input type="text" id="busqueda" onkeyup="buscarEstudiantes()" placeholder="Buscar por nombre o apellido...">
</form>

<div>
    <a href="?sort_by=nombre&order=ASC">Nombre Ascendente</a>
    <a href="?sort_by=nombre&order=DESC">Nombre Descendente</a>
    <a href="?sort_by=edad&order=ASC">Edad Ascendente</a>
    <a href="?sort_by=edad&order=DESC">Edad Descendente</a>
</div>

<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Edad</th>
            <th>Correo</th>
            <th>Rol</th>
            <th>Foto</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php while ($row = $result->fetch_assoc()): ?>
            <tr>
                <td><?php echo htmlspecialchars($row['id']); ?></td>
                <td>
                    <span title="Edad: <?php echo htmlspecialchars($row['edad']); ?>, Correo: <?php echo htmlspecialchars($row['correo']); ?>, Rol: <?php echo htmlspecialchars($row['rol']); ?>">
                        <?php echo htmlspecialchars($row['nombre']); ?>
                    </span>
                </td>
                <td><?php echo htmlspecialchars($row['edad']); ?></td>
                <td><?php echo htmlspecialchars($row['correo']); ?></td>
                <td><?php echo htmlspecialchars($row['rol']); ?></td>
                <td><img src="<?php echo htmlspecialchars($row['foto']); ?>" alt="Foto" style="width: 50px; height: 50px;"></td>
                <td>
                    <a href="Actualizar.php?id=<?php echo htmlspecialchars($row['id']); ?>">Actualizar</a>
                    <?php if ($rol === 'profesor'): ?>
                        <a href="Eliminar.php?id=<?php echo htmlspecialchars($row['id']); ?>">Eliminar</a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endwhile; ?>
    </tbody>
</table>

<?php if ($rol === 'profesor'): ?>
    <a href="Crear.php"><button class="button">Volver a Inicio</button></a>
<?php endif; ?>

<a href="Cerrar.php"><button class="button">Cerrar Sesión</button></a>

</body>
</html>
