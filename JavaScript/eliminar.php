<?php
session_start();
require 'config.php';

if (!isset($_SESSION['usuario_id']) || $_SESSION['rol'] !== 'profesor') {
    header('Location: Inicio_de_Sesion.php');
    exit;
}

$id = $_GET['id'];

$stmt = $conn->prepare("DELETE FROM estudiantes WHERE id = ?");
$stmt->bind_param('i', $id);
$stmt->execute();

header('Location: Ver.php');
exit;
?>