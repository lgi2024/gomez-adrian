<?php
$conn = mysqli_connect('localhost', 'alumno', '', 'escuela');

if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}

$sql = "SELECT id, nombre, edad, correo FROM estudiantes";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    echo "<table border='1'><tr><th>ID</th><th>Nombre</th><th>Edad</th><th>Correo Electrónico</th></tr>";
    while ($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td>" . $row["id"]. "</td><td>" . $row["nombre"]. "</td><td>" . $row["edad"]. "</td><td>" . $row["correo"]. "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 resultados";
}

mysqli_close($conn);
?>
