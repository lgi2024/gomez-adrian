<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    $name = $_POST['nombre'];
    $age = $_POST['edad'];
    $email = $_POST['correo'];

    $conn = mysqli_connect('localhost', 'alumno', '', 'escuela');

    if (!$conn) {
        die("Conexión fallida: " . mysqli_connect_error());
    }

    $sql = "UPDATE estudiantes SET nombre='$name', edad=$age, correo='$email' WHERE id=$id";

    if (mysqli_query($conn, $sql)) {
        echo "Estudiante actualizado con éxito";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
} else {
    $id = $_GET['id'];

    $conn = mysqli_connect('localhost', 'alumno', '', 'escuela');

    if (!$conn) {
        die("Conexión fallida: " . mysqli_connect_error());
    }

    $sql = "SELECT id, nombre, edad, correo FROM estudiantes WHERE id=$id";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $name = $row['nombre'];
        $age = $row['edad'];
        $email = $row['correo'];
    } else {
        echo "No se encontró el estudiante con ID: $id";
        exit();
    }

    mysqli_close($conn);
}
?>

<form method="post" action="">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    Nombre: <input type="text" name="nombre" value="<?php echo $name; ?>" required><br>
    Edad: <input type="number" name="edad" value="<?php echo $age; ?>" required><br>
    Correo electrónico: <input type="email" name="correo" value="<?php echo $email; ?>" required><br>
    <input type="submit" value="Actualizar">
</form>
