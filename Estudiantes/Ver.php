<?php
session_start();
require 'config.php';

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario_id'])) {
    header('Location: Inicio_de_sesion.php');
    exit;
}

$usuario_id = $_SESSION['usuario_id'];
$rol = $_SESSION['rol'];

// orden
$search = isset($_GET['search']) ? $_GET['search'] : '';
$sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'nombre';
$order = isset($_GET['order']) ? $_GET['order'] : 'ASC';

// Consulta para obtener los estudiantes
$sql = "SELECT id, nombre, edad, correo, rol, foto FROM estudiantes WHERE nombre LIKE ? ORDER BY $sort_by $order";
$stmt = $conn->prepare($sql);
$search_param = "%" . $search . "%";
$stmt->bind_param('s', $search_param);
$stmt->execute();
$result = $stmt->get_result();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Lista de Estudiantes</title>
    <style>
        body {
            background-color: #f0f0f0;
            text-align: center;
            padding: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            padding: 10px;
            border: 1px solid #ddd;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        .button {
            background-color: #4CAF50;
            color: white;
            border: none;
            padding: 10px 20px;
            text-align: center;
            font-size: 16px;
            cursor: pointer;
            border-radius: 5px;
            margin: 10px;
        }
    </style>
</head>
<body>

<h1>Lista de Estudiantes</h1>

<form method="GET" action="Ver.php">
    <input type="text" name="search" value="<?php echo htmlspecialchars($search); ?>" placeholder="Buscar por nombre">
    <input type="submit" value="Buscar">
</form>

<div>
    <a href="?sort_by=nombre&order=ASC">Nombre Ascendente</a>
    <a href="?sort_by=nombre&order=DESC">Nombre Descendente</a>
    <a href="?sort_by=edad&order=ASC">Edad Ascendente</a>
    <a href="?sort_by=edad&order=DESC">Edad Descendente</a>
</div>

<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Edad</th>
            <th>Correo</th>
            <th>Rol</th>
            <th>Foto</th>
            <th>Acciones</th> <!-- Añadido solo una columna de Acciones -->
        </tr>
    </thead>
    <tbody>
        <?php while ($row = $result->fetch_assoc()): ?>
            <tr>
                <td><?php echo htmlspecialchars($row['id']); ?></td>
                <td><?php echo htmlspecialchars($row['nombre']); ?></td>
                <td><?php echo htmlspecialchars($row['edad']); ?></td>
                <td><?php echo htmlspecialchars($row['correo']); ?></td>
                <td><?php echo htmlspecialchars($row['rol']); ?></td>
                <td><img src="<?php echo htmlspecialchars($row['foto']); ?>" alt="Foto" style="width: 50px; height: 50px;"></td>
                <td>
                    <a href="Actualizar.php?id=<?php echo htmlspecialchars($row['id']); ?>">Actualizar</a>
                    <?php if ($rol === 'profesor'): ?>
                        <a href="Eliminar.php?id=<?php echo htmlspecialchars($row['id']); ?>">Eliminar</a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endwhile; ?>
    </tbody>
</table>

<?php if ($rol === 'profesor'): ?>
    <a href="Crear.php"><button class="button">Volver a Inicio</button></a>
<?php endif; ?>

<a href="Cerrar.php"><button class="button">Cerrar Sesión</button></a>

</body>
</html>
