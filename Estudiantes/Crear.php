<?php
session_start();
require 'config.php';

// Verificar si el usuario está autenticado y tiene el rol de 'profesor'
if (!isset($_SESSION['usuario_id']) || $_SESSION['rol'] !== 'profesor') {
    header('Location: Inicio.php');
    exit;
}

// Carpeta de subida
$upload_dir = 'Estudiantes/';
if (!is_dir($upload_dir)) {
    mkdir($upload_dir, 0777, true);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nombre = $_POST['nombre'];
    $edad = $_POST['edad'];
    $usuario = $_POST['usuario'];
    $correo = $_POST['correo'];
    $rol = $_POST['rol'];
    $contraseña = password_hash($_POST['contraseña'], PASSWORD_DEFAULT);
    
    // Validar el rol
    if (!in_array($rol, ['alumno', 'profesor'])) {
        header('Location: Crear.php?message=Error%20en%20los%20datos');
        exit;
    }

    // Inicializar la variable de la foto
    $foto = NULL;

    if (isset($_FILES['foto']) && $_FILES['foto']['error'] === UPLOAD_ERR_OK) {
        $foto_tmp_name = $_FILES['foto']['tmp_name'];
        $foto_name = basename($_FILES['foto']['name']);
        $foto_path = $upload_dir . $foto_name;

        if (move_uploaded_file($foto_tmp_name, $foto_path)) {
            $foto = $foto_path;
        }
    }

    // Insertar el nuevo estudiante o profesor con la contraseña
    $stmt = $conn->prepare("INSERT INTO estudiantes (nombre, edad, usuario, correo, rol, contraseña, foto) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param('sisssss', $nombre, $edad, $usuario, $correo, $rol, $contraseña, $foto);

    $stmt->execute();
    $stmt->close();

    header('Location: Crear.php?message=success');
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Gestión de Estudiantes</title>
    <style>
        body {
            background-color: #f0f0f0;
            text-align: center;
            padding: 20px;
        }
        form {
            margin: 0 auto;
            display: inline-block;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        input[type="submit"], button {
            background-color: #4CAF50; /* Verde claro */
            color: white;
            border: none;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 10px;
            cursor: pointer;
            border-radius: 5px;
        }
        input[type="text"], input[type="number"], input[type="email"], input[type="password"] {
            border: 1px solid #ddd;
            padding: 8px;
            margin: 8px 0;
            border-radius: 4px;
        }
        .message-success {
            margin: 10px 0;
            color: #4CAF50; /* Verde claro para mensajes exitosos */
        }
        .message-error {
            margin: 10px 0;
            color: #F44336; /* Rojo para mensajes de error */
        }
        .button {
            background-color: #00BFFF; /* Celeste */
            color: white;
            border: none;
            padding: 10px 20px;
            font-size: 16px;
            cursor: pointer;
            border-radius: 5px;
            margin: 10px;
        }
        .logout-button {
            background-color: #F44336; /* Rojo para el botón de cerrar sesión */
            color: white;
            border: none;
            padding: 10px 20px;
            text-align: center;
            font-size: 16px;
            cursor: pointer;
            border-radius: 5px;
            margin: 10px;
        }
        .header-buttons {
            text-align: right; /* Alinear botones a la derecha */
            margin-bottom: 20px;
        }
    </style>
</head>
<body>

<h1>Gestión de Estudiantes</h1>

<!-- Botones en la parte superior derecha -->
<div class="header-buttons">
    <form action="Ver.php" method="GET" style="display: inline;">
        <button type="submit" class="button">Ver lista de alumnos</button>
    </form>
    <form action="Cerrar.php" method="POST" style="display: inline;">
        <button type="submit" class="logout-button">Cerrar Sesión</button>
    </form>
</div>

<h2>Crear Estudiante</h2>
<form method="post" action="Crear.php" enctype="multipart/form-data">
    Nombres: <input type="text" name="nombre" required><br>
    Edad: <input type="number" name="edad" required><br>
    Correo electrónico: <input type="email" name="correo" required><br>
    Usuario: <input type="text" name="usuario" required><br>
    Contraseña: <input type="password" name="contraseña" required><br>
    Rol: 
    <select name="rol" required>
        <option value="alumno">Alumno</option>
        <option value="profesor">Profesor</option>
    </select><br>
    Foto (opcional): <input type="file" name="foto"><br>
    <input type="submit" name="crear" value="Crear">
</form>

<?php
if (isset($_GET['message'])) {
    if (strpos($_GET['message'], 'Error') !== false) {
        echo "<p class='message-error'>" . htmlspecialchars($_GET['message']) . "</p>";
    } else {
        echo "<p class='message-success'>" . htmlspecialchars($_GET['message']) . "</p>";
    }
}
?>

<br>
<a href="Crear.php"><button class="button">Volver a Inicio</button></a>

</body>
</html>
