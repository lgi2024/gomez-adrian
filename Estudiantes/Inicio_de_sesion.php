<?php 
session_start();
require 'config.php';

// inicio de sesión
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $usuario = $_POST['usuario'];
    $contraseña = $_POST['contraseña'];

    // Preparar y ejecutar la consulta para recuperar el usuario
    $stmt = $conn->prepare("SELECT id, rol, contraseña FROM estudiantes WHERE usuario = ?");
    $stmt->bind_param('s', $usuario);
    $stmt->execute();
    $stmt->bind_result($id, $rol, $contraseña_db);
    $stmt->fetch();
    $stmt->close();

    // Verificar si las credenciales coinciden
    if ($contraseña === $contraseña_db) {
        $_SESSION['usuario_id'] = $id;
        $_SESSION['rol'] = $rol;

        // Redirigir según el rol
        if ($rol === 'profesor') {
            header('Location: Crear.php');
        } else {
            header('Location: Ver.php');
        }
        exit;
    } else {
        $error = 'Usuario o contraseña incorrectos.';
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Inicio de Sesión</title>
    <style>
        body {
            background-color: #f0f0f0;
            text-align: center;
            padding: 20px;
        }
        form {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            display: inline-block;
            width: 300px;
        }
        input, button {
            margin-bottom: 10px;
            padding: 10px;
            width: 100%;
            border-radius: 5px;
            border: 1px solid #ddd;
        }
        button {
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }
        .error {
            color: red;
            margin-bottom: 10px;
        }
    </style>
</head>
<body>

<h1>Inicio de Sesión</h1>

<form method="POST">
    <?php if (isset($error)): ?>
    <div class="error"><?php echo htmlspecialchars($error); ?></div>
    <?php endif; ?>
    <input type="text" name="usuario" placeholder="Usuario" required>
    <input type="password" name="contraseña" placeholder="Contraseña" required>
    <button type="submit">Iniciar Sesión</button>
</form>

</body>
</html>
