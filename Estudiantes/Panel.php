<?php
session_start();
if (!isset($_SESSION['usuario_id'])) {
    header('Location: Inicio.php');
    exit;
}

if ($_SESSION['rol'] === 'profesor') {
    echo "<a href='Crear.php'>Crear Alumno</a><br>";
}

echo "<a href='Ver.php'>Ver Alumnos</a><br>";
echo "<a href='logout.php'>Cerrar Sesión</a>";
?>
