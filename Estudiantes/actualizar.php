<?php
session_start();
require 'config.php';

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario_id'])) {
    header('Location: Inicio_de_sesion.php');
    exit;
}

$id = $_GET['id'];

// Procesar la actualización
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nombre = $_POST['nombre'];
    $edad = $_POST['edad'];
    $usuario = $_POST['usuario'];
    $correo = $_POST['correo'];
    $foto = $_FILES['foto']['name'];

    // Subir la foto
    if ($foto) {
        $target_dir = "Estudiantes/";
        $target_file = $target_dir . basename($_FILES["foto"]["name"]);
        move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file);
    }

    // Actualizar en la base de datos
    $stmt = $conn->prepare("UPDATE estudiantes SET nombre = ?, edad = ?, usuario = ?, correo = ?, foto = ? WHERE id = ?");
    $stmt->bind_param('sisssi', $nombre, $edad, $usuario, $correo, $foto, $id);
    $stmt->execute();

    header('Location: Ver.php');
    exit;
} else {
    $stmt = $conn->prepare("SELECT nombre, edad, usuario, correo, foto FROM estudiantes WHERE id = ?");
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $stmt->bind_result($nombre, $edad, $usuario, $correo, $foto);
    $stmt->fetch();
    $stmt->close();
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Actualizar Estudiante</title>
    <style>
        body {
            background-color: #f0f0f0;
            text-align: center;
            padding: 20px;
        }
        form {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            display: inline-block;
            width: 300px;
        }
        input, button {
            margin-bottom: 10px;
            padding: 10px;
            width: 100%;
            border-radius: 5px;
            border: 1px solid #ddd;
        }
        button {
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }
    </style>
</head>
<body>

<h1>Actualizar Estudiante</h1>

<form method="POST" enctype="multipart/form-data">
    <input type="text" name="nombre" value="<?php echo htmlspecialchars($nombre); ?>" placeholder="Nombre Completo" required>
    <input type="number" name="edad" value="<?php echo htmlspecialchars($edad); ?>" placeholder="Edad" required>
    <input type="text" name="usuario" value="<?php echo htmlspecialchars($usuario); ?>" placeholder="Usuario" required>
    <input type="email" name="correo" value="<?php echo htmlspecialchars($correo); ?>" placeholder="Correo" required>
    <input type="file" name="foto">
    <button type="submit">Actualizar</button>
</form>

<br>
<a href="Ver.php"><button>Volver</button></a>

</body>
</html>

